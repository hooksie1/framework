module gitlab.com/hooksie1/framework

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.9.0
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/tools v0.1.6 // indirect
)
