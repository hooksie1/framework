/*
Copyright © 2021 John Hooks john@hooks.technology

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package tpl

func Main() []byte {
	return []byte(`
package main

import (
	"gitlab.com/hooksie1/framework/api"
	"net/http"
)

func withError(w http.ResponseWriter, r *http.Request) error {
	return api.NewHTTPError(nil, 404, "uh oh")
}

func normal(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
	w.Write([]byte("works!"))
}

func main() {
	r := api.NewRouter()

	apiRouter := r.PathPrefix("/v1").Subrouter().StrictSlash(true)
	apiRouter.Handle("/error", api.ErrHandler(withError)).Methods("GET")
	apiRouter.HandleFunc("/normal", normal).Methods("GET")

	api.Serve(r)

}
`)
}

func Dockerfile() []byte {
	return []byte(`
FROM golang:alpine as builder
WORKDIR /app
RUN apk update && apk upgrade && apk add --no-cache ca-certificates
RUN update-ca-certificates
ADD . /app/
RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags="-s -w" -installsuffix cgo -o {{ .Name }} .


FROM scratch

COPY --from=builder /app/{{ .Name }} .
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

CMD ["./{{ .Name }}", "start"]
`)
}

func Makefile() []byte {
	return []byte(`
PROJECT_NAME := "{{ .Name }}"
PKG := "gitlab.com/hooksie1/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
VERSION := $$(git describe --tags | cut -d '-' -f 1)

.PHONY: all build docker dep clean test coverage lint

all: build

lint: ## Lint the files
		@golint -set_exit_status ./...

test: ## Run unittests
		@go test ./...

coverage:
		@go test -cover ./...
		@go test -coverprofile=cover.out && go tool cover -html=cover.out -o coverage.html

dep: ## Get the dependencies
		@go get -u golang.org/x/lint/golint

build: dep ## Build the binary file
		@CGO_ENABLED=0 GOOS=linux go build -a -ldflags "-w -X '$(PKG)/cmd.Version=$(VERSION)'" -o $(PROJECT_NAME)
		@CGO_ENABLED=0 GOOS=windows go build -a -ldflags "-w -X '$(PKG)/cmd.Version=$(VERSION)'" -o $(PROJECT_NAME).exe
		@CGO_ENABLED=0 GOOS=darwin go build -a -ldflags "-w -X '$(PKG)/cmd.Version=$(VERSION)'" -o $(PROJECT_NAME)_darwin

docker: build
		@docker build -f Dockerfile.app -t hooksie1/$(PROJECT_NAME):$(VERSION) .

push: docker
		@docker push hooksie1/$(PROJECT_NAME):$(VERSION)

clean: ## Remove previous build
		git clean -fd
		git clean -fx
		git reset --hard

help: ## Display this help screen
		@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	`)
}

func Gitignore() []byte {
	return []byte(`
{{ .Name }}
{{ .Name }}.exe
{{ .Name }}_darwin

*.test
`)
}

func GitLabCI() []byte {
	return []byte(`
image: golang

stages:
  - test
  - build

before_script:
  - make dep

unit_tests:
  stage: test
  script:
    - make test

code_coverage:
  stage: test
  script:
    - make coverage
  artifacts:
    paths:
      - coverage.html

lint_code:
  stage: test
  script:
    make lint
  allow_failure: true

build:
  stage: build
  script:
    - make build
  only:
    - tags
  artifacts:
    paths:
      - {{ .Name }}
      - {{ .Name }}.exe
      - {{ .Name }}_darwin
`)
}
