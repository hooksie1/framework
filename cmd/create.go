/*
Copyright © 2021 John Hooks john@hooks.technology

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"html/template"
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/hooksie1/framework/cmd/tpl"
)

// createCmd represents the create command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a new API",
	Run:   createProject,
}

type Project struct {
	Name     string
	Path     string
	FullPath string
}

func init() {
	rootCmd.AddCommand(createCmd)

	createCmd.Flags().StringP("project-name", "n", "", "The name for the project")
	createCmd.MarkFlagRequired("project-name")
	createCmd.Flags().StringP("project-path", "p", ".", "The path to put the project")
	viper.BindPFlag("project-name", createCmd.Flags().Lookup("project-name"))
	viper.BindPFlag("project-path", createCmd.Flags().Lookup("project-path"))
}

func createProject(cmd *cobra.Command, args []string) {
	proj := Project{
		Name:     viper.GetString("project-name"),
		Path:     viper.GetString("project-path"),
		FullPath: fmt.Sprintf("%s/%s", viper.GetString("project-path"), viper.GetString("project-name")),
	}

	if _, err := os.Stat(proj.FullPath); os.IsNotExist(err) {
		if err := os.MkdirAll(proj.FullPath, 0755); err != nil {
			log.Printf("error creating path: %s", err)
			os.Exit(1)
		}
	}

	if err := proj.createProject(); err != nil {
		log.Printf("error creating project: %s", err)
		os.Exit(1)
	}

}

func (p *Project) createProject() error {
	if err := p.createMain(); err != nil {
		return err
	}

	return nil
}

func (p *Project) createMain() error {
	if err := p.createFile("main.go", tpl.Main()); err != nil {
		return err
	}

	return p.createDockerfile()
}

func (p *Project) createDockerfile() error {
	if err := p.createFile("Dockerfile", tpl.Dockerfile()); err != nil {
		return err
	}

	return p.createMakefile()
}

func (p *Project) createMakefile() error {
	if err := p.createFile("Makefile", tpl.Makefile()); err != nil {
		return err
	}

	return p.createGitIgnore()
}

func (p *Project) createGitIgnore() error {
	if err := p.createFile(".gitignore", tpl.Gitignore()); err != nil {
		return err
	}

	return p.createCI()
}

func (p *Project) createCI() error {
	return p.createFile(".gitlab-ci.yml", tpl.GitLabCI())
}

func (p *Project) createFile(n string, b []byte) error {
	f, err := os.Create(fmt.Sprintf("%s/%s", p.FullPath, n))
	if err != nil {
		return fmt.Errorf("error creating file: %s", err)
	}

	defer f.Close()

	temp := template.Must(template.New(n).Parse(string(b)))
	err = temp.Execute(f, p)
	if err != nil {
		return fmt.Errorf("error executing template: %s", err)
	}

	return nil
}
